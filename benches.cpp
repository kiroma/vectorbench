#include <vector>
#include <chrono>
#include "benches.hpp"
#include <random>

void pre(int f)
{
	std::cout << "Using the pre-initialization method" << std::endl;
	auto a = std::chrono::high_resolution_clock::now();
	std::vector<int> vec;
	vec.reserve(f);
	std::cout << "Size of vec before operations: " << vec.size() << ", Capacity: " << vec.capacity() << std::endl;
	for(int i = 0; i < f; ++i)
	{
		vec.push_back(random());
	}
	auto b = std::chrono::high_resolution_clock::now();
	std::cout << "Size of vec after operations: " << vec.size() << ", Capacity: " << vec.capacity() << std::endl;
	std::chrono::duration<double> dur = b-a;
	std::cout << "Filling a vector of size " << f << " took " << dur.count() << " seconds." << std::endl;
}

void live(int f)
{
	std::cout << "Using the live-relocation method" << std::endl;
	auto a = std::chrono::high_resolution_clock::now();
	std::vector<int> vec;
	std::cout << "Size of vec before operations: " << vec.size() << ", Capacity: " << vec.capacity() << std::endl;
	for(int i = 0; i < f; ++i)
	{
		vec.push_back(random());
	}
	auto b = std::chrono::high_resolution_clock::now();
	std::cout << "Size of vec after operations: " << vec.size() << ", Capacity: " << vec.capacity() << std::endl;
	std::chrono::duration<double> dur = b-a;
	std::cout << "Filling a vector of size " << f << " took " << dur.count() << " seconds." << std::endl;
}
