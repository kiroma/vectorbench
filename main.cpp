#include <cstring>
#include "benches.hpp"

void print_usage()
{
	std::cout << "VectorBench [mode] [size]\n"
	<< "Possible modes are:\n"
	<< "\"--pre\", preallocates vector before filling it.\n"
	<< "\"--live\", allocates vector spaces while filling it." << std::endl;
}

int main(int argc, char **argv) {
    switch(argc)
	{
		case 3:
			if(!std::strcmp(argv[1], "--pre"))
			{
				pre(atoi(argv[2]));
			}
			else
			{
				if(!std::strcmp(argv[1], "--live"))
				{
					live(atoi(argv[2]));
				}
				else
				{
					print_usage();
				}
			}
			break;
		default:
			print_usage();
	}
    return 0;
}
